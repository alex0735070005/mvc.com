<?php

namespace model;

class LoginModel extends Model
{
    public function getUserByEmail($email)
    {
        $db = $this->getDB();
        
        $result = $db->select(['login', 'password', 'email', 'role'], ['email'=> $email], 'users');
        
        return $result[0];
    }
}