<?php

namespace model;

class RegistrationModel extends Model
{
    public function register($data)
    {
        $db = $this->getDB();
        
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $data['role'] = 'ROLE_USER';
        
        try {
            if($db->insert($data, 'users')){
                return ['status'=>true, 'message'=>'You registration success !'];
            }
        } 
        catch (Exception $ex) 
        {
            return ['status'=>false, 'errors'=>['email_exists' => 'Email already exists !']];
        }
    }
}