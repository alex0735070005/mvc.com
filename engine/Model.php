<?php

namespace engine;

abstract class Model 
{
    public function getDB()
    {
        include '../system/settings.php';
     
        return DB::getInstance($dbparams);
        
    }
}
