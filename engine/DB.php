<?php

namespace engine;

class DB {
    
    private $db;
    
    private static $instance = null;

    public function __construct($params) 
    {
        // var_dump($params); die();
        try {
            $this->db = new PDO("mysql:host={$params['host']};dbname={$params['name']};charset={$params['charset']}", $params['user'], $params['pass']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e) {
           
            echo $e->getMessage();  
        }
    }
    
    public function insert(Array $data, $name)
    {
        $colums = '';
        $rows = '';
        
        $j = 0;
       
        foreach ($data as $key => $value)
        {
            if($j === 0){
                $colums .= $key;
                $rows   .= ':' . $key;
            }
            else{
                $colums .= ', ' . $key;
                $rows   .= ', :' . $key;
            }
            $j++;
        }  
        
        $sql = "INSERT INTO {$name} ({$colums}) VALUES ({$rows})";
               
        $stmt = $this->db->prepare($sql);

        return $stmt->execute($data);
    }
    
    public function select(Array $dataColums, $dataWhere = [], $name)
    {
        $colums = implode(',', $dataColums);
        $where  = '';
        $values = [];
        
        $j = 0;
       
        foreach ($dataWhere as $key => $value)
        {
            $values[] = $value;
            
            if($j === 0){
                $where .= $key . '= ?';
            }
            else{
                $where .= ' AND ' . $key . '= ?';
            }
            $j++;
        }  
        
        if($dataWhere){
             $stmt = $this->db->prepare("SELECT {$colums} FROM {$name} WHERE {$where}");
        }else{
            $stmt = $this->db->prepare("SELECT {$colums} FROM {$name}");
        }
      
        $stmt->execute($values);
       
        return $stmt->fetchAll();
        
    }
    
    
    public static function getInstance($params)
    {
        //var_dump($params); die();
        if(self::$instance === null)
        {     
            self::$instance = new self($params);
             
            return self::$instance;
        } 
        return self::$instance;
    }
}