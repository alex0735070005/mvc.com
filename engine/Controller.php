<?php

namespace engine;

use controller\HeaderController;
use controller\FooterController;

abstract class Controller {
    
    protected function getRequest()
    {
        return Request::getInstance();
    }

    protected function LoadHeader (Array $data = [])
    {
        $Header = new HeaderController();
        $Header->index($data);
    }
    
    protected function LoadFooter ()
    {
        $Footer = new FooterController();
        $Footer->index();
    }
    
    protected function render ($path, Array $data = [])
    {
        extract($data);
        
        include $path;
    }
    
    protected function getUser()
    {
        if(isset($_SESSION['user']))
        {
           return $_SESSION['user'];
        }
        return false;
    }
    
    protected function isAdmin()
    {
        $user = $this->getUser();
        
        if($user && $user['role'] === 'ROLE_ADMIN'){
            return true;
        }
        
        return false;
    }
    
    protected function logoutUser()
    {
        //Remove session vars
        $_SESSION = array();

        //Remove session cookies
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

       //Remove session
        session_destroy();
    }
}