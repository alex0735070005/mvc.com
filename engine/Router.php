<?php

namespace engine;

final class Router {
    
    private $Request;
    private $routes;
    private $route;
    private $path;
    private $params;
    private $Controller;
    private $action;

    /*
     * Params: Request
     */
    public function __construct(Request $Request) 
    {
        // set request object
        $this->Request = $Request;
        
        // set request array routes
        $this->routes  = ROUTES;
    }
    
    /*
     * Init route and action
     */
    public function initRoute()
    {
        // get route string from get array global
        $this->path = $this->Request->get('get', 'route');
        
        // If isset string path and isset route in ROUTES array
        if($this->path && $this->setRoute())
        {
            // Create controller from current route
            $this->Controller = new $this->route['namespace']();
            
            // Set string action name
            $action = $this->route['action'];
                    
            // Init current action from controller
            //If isset slug in route
            if(isset($this->route['slug']))
            {
                // Init action with slug
                $this->Controller->$action(end($this->params));
            
            } else {
                // Init action
                $this->Controller->$action();
            }
           
        }
        // If isset string url and is not route in ROUTES
        elseif($this->path && !$this->route)
        {
            // Page page not found
            echo '404 page not found';
        }
        else{
            
            $this->route = $this->routes['home'];
            
            $this->Controller = new $this->route['namespace']();
            
            $action = $this->route['action'];
                    
            $this->Controller->$action();
        }
    }
    
    private function setRoute()
    {
        // Delimit url string on array and remove slash on last element route
        $this->params = explode('/', trim($this->path, '/'));
        
        // Set params from params object
        $params = $this->params;
        
        // If isset full url string as key in ROUTES 
        if(isset($this->routes[$this->path]))
        {
            $this->route = $this->routes[$this->path];
            return true;
        }

        // Sort params url
        foreach ($params as $key => $param)
        {
            // If first step and key is 0
            if(!$key)
            {
                // Name is full url string
                $name = implode('/', $params);
            }
            else{
                // Name is url string delete last element 
                array_pop($params);
                $name = implode('/', $params);
            } 
            
            // If isset name as key in ROUTES
            if(isset($this->routes[$name]))
            {
                // Set route in ROUTES array
                $this->route = $this->routes[$name];  break;
            }
        }
        // If isset route return true
        return $this->route ? true : false;
    }
    
}
