<?php

namespace engine;

class Request 
{
    private $get;
    private $post;
    private $files;
    private $server;
    private $session;


    private static $instance = null;


    public function __construct() 
    {
        $this->get      = $_GET;
        $this->post     = $_POST;
        $this->files    = $_FILES;
        $this->server   = $_SERVER;
        $this->session  = $_SESSION;
    }
    
    /*
     * Params: string type (POST, GET, FILES), string key
     */
    public function get($type, $key) 
    {
        // Get type from Request
        $result = $this->$type;
        
        // If isset value in array by key
        if(isset($result[$key]))
        {
            // return array value
            return $result[$key];
        }
        // If not isset value in array by key
        return false;
    }
    
    public function getAll($type) 
    {
        return $this->$type;
    }
    
    public function getJsonData($type) 
    {
        return json_decode(file_get_contents("php://input"), $type);
    }
    
    public function isPost()
    {
        if($this->server['REQUEST_METHOD'] === 'POST'){
            return true;
        }
        return false;
    }
    
    public function isGet()
    {
        if($this->server['REQUEST_METHOD'] === 'GET'){
            return true;
        }
        return false;
    }

    public static function getInstance()
    {
        if(self::$instance === null)
        {     
            self::$instance = new self();
            
            return self::$instance;
        } 
        return self::$instance;
    }

}
