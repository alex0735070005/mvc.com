function sendRegistration()
{
    var login    = document.querySelector('#InputLogin');
    var email    = document.querySelector('#InputEmail');
    var password = document.querySelector('#InputPassword');
    var alerts    = document.querySelector('#message');
    
    var User = {};
    
    User.login    = login.value;
    User.email    = email.value;
    User.password = password.value;
    
    //console.log(User);
    
    fetch('/registration', 
    {
        method:'POST',
        body:JSON.stringify(User)
    }).then(function(responce)
    {
        return responce.json();
        
    }).then(function(data)
    {
        if(data.status)
        {
            alerts.innerHTML = data.message;
            alerts.className = 'alert alert-success';
            
            setTimeout(function(){
                alerts.className = 'alert alert-success d-none';
            }, 2000);
            
        }
        else
        {
            var str = '';
            
            for(var j in data.errors)
            {
                str += ' ' + data.errors[j];
            }
            
            alerts.innerHTML = str;
            
            alerts.className = 'alert alert-danger';
            
            setTimeout(function(){
                alerts.className = 'alert alert-danger d-none';
            }, 2000);
        }
    });
    
}

var sendButtonRegistration = document.querySelector('#sendButtonRegistration');

sendButtonRegistration.onclick = sendRegistration;