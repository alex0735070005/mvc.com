<?php

const ROUTES = 
[
    'home' => [
        'controller' => 'HomeController',
        'path'       => '../controller/HomeController.php',
        'namespace'  => '\controller\HomeController',
        'action'     => 'index',
    ],
    
    'page' => [
        'controller' => 'PageController',
        'path'       => '../controller/PageController.php',
        'namespace'  => '\controller\PageController',
        'action'     => 'index',
        'slug'       => [
            'reg'    => '^[0-9a-zA-Z-]{2,25}$'
        ]
    ],
    
    'registration' => [
        'controller' => 'RegistrationController',
        'path'       => '../controller/RegistrationController.php',
        'namespace'  => '\controller\RegistrationController',
        'action'     => 'index'
    ],
    
    'login' => [
        'controller' => 'LoginController',
        'path'       => '../controller/LoginController.php',
        'namespace'  => '\controller\LoginController',
        'action'     => 'index'
    ],
    
    'logout' => [
        'controller' => 'LoginController',
        'path'       => '../controller/LoginController.php',
        'namespace'  => '\controller\LoginController',
        'action'     => 'logout'
    ],
    
    'admin' => [
        'controller' => 'AdminController',
        'path'       => '../controller/AdminController.php',
        'namespace'  => '\controller\AdminController',
        'action'     => 'index'
    ]
    
];