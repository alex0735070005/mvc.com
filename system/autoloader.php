<?php

function autoloader($namespase) 
{
    require '../'. str_replace('\\', '/', $namespase).'.php';
}

spl_autoload_register('autoloader');