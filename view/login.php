<h1>Login</h1>
<form id="login" action="/login" method="POST">   
    <div class="form-group">
        <label for="InputEmail">Email address</label>
        <input value="<?= $data['email'] ? $data['email']: '' ?>" name="email" type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="InputPassword">Password</label>
        <input name="password" type="password" class="form-control" id="InputPassword" placeholder="Password">
    </div>
    <?php if($error){ ?>
        <div id="message" class="alert alert-danger" role="alert">
            Not valid password or email !!!
        </div>
    <?php } ?>
    <button class="btn btn-primary">Submit</button>
</form>