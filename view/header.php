<html>
    <head>
        <title><?= $title ?></title>
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/main.css" />
        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>
        <script defer src="/js/main.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand d-lg-none" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/page/about-us">About us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/page/catalog">Catalog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/page/gallery">Gallery</a>
                        </li>
                        
                    </ul>
                    
                    <?php if($user){ ?>
                        <ul class="navbar-nav mr-auto pull-right">
                            <li class="nav-item">
                                <a href="#" class="nav-link" > Hello: <?= $user['login'] ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/logout">Logout</a>
                            </li>
                            <?php if($user['role'] === 'ROLE_ADMIN'){ ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="/admin">Admin</a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    
                    <?php if(!$user){ ?>
                        <ul class="navbar-nav mr-auto pull-right">
                            <li class="nav-item">
                                <a class="nav-link" href="/login">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/registration">Registration</a>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
            </nav>
