<h1>Registration</h1>
<form id="registration" action="/registration" method="POST">
    <div class="form-group">
        <label for="InputLogin">Login</label>
        <input  name="login" type="text" class="form-control" id="InputLogin" aria-describedby="loginHelp" placeholder="Enter login">
    </div>
    <div class="form-group">
        <label for="InputEmail">Email address</label>
        <input name="email" type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="InputPassword">Password</label>
        <input name="password" type="password" class="form-control" id="InputPassword" placeholder="Password">
    </div>
    <div id="message" class="alert alert-success d-none" role="alert">
         
    </div>
    <button id="sendButtonRegistration" type="button" class="btn btn-primary">Submit</button>
</form>