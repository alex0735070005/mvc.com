<?php

namespace controller;


class RegistrationController extends Controller
{
    
    public function index()
    {
        
        $Request = $this->getRequest();
        
        
        if($Request->isPost())
        {
            
            $data = $Request->getJsonData(true);
            
            $errors = $this->valid($data);
           
            if(!$errors){
            
                $model = new RegistrationModel();
                
                $answer = $model->register($data);
                
                
            }else{
                $answer = ['status'=> false, 'errors' => $errors];
            }
            
            echo json_encode($answer); die;
        }
        
       
        $this->LoadHeader(['title' => 'Registration']);
        
        
        $this->render('../view/registration.php', [
            'name' => 'Vasa'
        ]);
        
        
        $this->LoadFooter();
    }
    
    private function valid($data)
    {
        $errors = [];
        
        if(!isset($data['login']) || !preg_match('/^[a-zA-Z0-9]{0,25}$/', $data['login']))
        {
            $errors['login'] = "Not valid login !";
        }
        
        if(!isset($data['email']) || !preg_match('/\A[^@]+@([^@\.]+\.)+[^@\.]+\z/', $data['email']))
        {
            $errors['email'] = "Not valid email";
        }
        
        if(!isset($data['password']) || !preg_match('/^[a-zA-Z0-9-_\.{}*]{6,25}$/', $data['password']))
        {
            $errors['password'] = "Not valid password !";
        }
        
        return $errors;
    }
   
}