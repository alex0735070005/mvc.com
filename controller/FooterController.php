<?php

namespace controller;

use engine\Controller;

class FooterController extends Controller
{
    function index()
    {
        $this->render('../view/footer.php');
    }
}
