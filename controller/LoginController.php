<?php

namespace controller;

class LoginController extends Controller
{
    
    public function index()
    {
        
        $Request = $this->getRequest();
        
        $Model = new LoginModel(); 
        
        if($Request->isPost())
        { 
            $Model = new LoginModel();  
            
            $data = $Request->getAll('post');
            
            $user = $Model->getUserByEmail($data['email']);
            
            $error = false;
            
            if(password_verify($data['password'], $user['password']))
            {
                header('Location: /');
                $_SESSION['user'] = $user;
            }
            else{
                $error = true;
            }
        }
        
       
        $this->LoadHeader(['title' => 'Login']);
        
        
        $this->render('../view/login.php', ['error'=>$error, 'data'=>$data]);
        
        
        $this->LoadFooter();
    }
    
    public function logout()
    {
        $this->logoutUser();
        header('Location: /login');
    }
    
}