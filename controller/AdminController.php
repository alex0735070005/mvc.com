<?php

namespace controller;

class AdminController extends Controller {
    
    public function index()
    {
        $this->LoadHeader();
        
        if($this->isAdmin())
        {
            $this->render('../view/admin.php');
        } else {
            $this->render('../view/401.php');
        }
        
        $this->LoadFooter();
        
    }
    
}