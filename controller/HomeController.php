<?php

namespace controller;

use engine\Controller;

class HomeController extends Controller{
    
    public function index()
    {
      
        $this->LoadHeader();
        
        $this->render('../view/home.php');
        
        $this->LoadFooter();
    }
    
}