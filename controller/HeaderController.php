<?php

namespace controller;

use engine\Controller;

class HeaderController extends Controller
{
    function index($data)
    {
        $user = $this->getUser();
        
        $this->render('../view/header.php', [
            'title' => $data['title'],
            'user'  => $user
        ]);
    }
}
