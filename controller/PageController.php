<?php

namespace controller;

class PageController extends Controller
{
    
    public function index($slug)
    {
       
        $this->LoadHeader(['title' => $slug]);
        
        
        
        $this->LoadFooter();
    }
    
   
}